window.onload = () => {
  formatTables()
  let pages = document.body.querySelector(".page")
  let pageHeight = pages.clientHeight
  let pagecount = 1
  document.body.appendChild(newPageFooter(pagecount))
  fillPages(pages, pageHeight, pagecount)
  fillFooterTotalPages()
}

const fillPages = (page, pageHeight, pagecount) => {
  let pageNodes = [...page.children]
  if (pageNodes[0].tagName.toLowerCase() === 'table' && pageNodes[0].className === 'fauxRow') {
    pageNodes[0].children[0].children[0].children[0].children[0].remove()
  }
  let elementsHeight = 0
  const splitPageElement = pageNodes.findIndex((el)=>{
    elementsHeight += el.offsetHeight
    return elementsHeight > pageHeight
  })

  if (splitPageElement === -1) return

  pageNodes = pageNodes.slice(splitPageElement)
  const pageCarrier = newPagePart('page')
  pageNodes.forEach((node)=>pageCarrier.appendChild(node))
  document.body.appendChild(pageCarrier)
  document.body.appendChild(newPageFooter(++pagecount))
  fillPages(pageCarrier, pageHeight, pagecount)
}

const newPagePart = (partName) => {
  const part = document.createElement('div')
  part.classList.add(partName)
  return part
}

const newPageFooter = (pageNum) => {
  const footer = newPagePart('footer');
  const name = newPagePart('td1')
  name.innerHTML = 'Peter Muster (01.11.1943)'
  const title = newPagePart('td2')
  title.innerHTML = 'eMediplan by Softwarehersteller AG (V1.0)'
  const pages = newPagePart('td3')
  pages.innerHTML = `Seite ${pageNum} von `
  footer.appendChild(name)
  footer.appendChild(title)
  footer.appendChild(pages)
  return footer
}

const fillFooterTotalPages = () => {
  let paginationFooters = [...document.getElementsByClassName('td3')]
  paginationFooters.forEach((el) => el.innerHTML += document.getElementsByClassName('page').length)
}

const formatTables = () => {
  const tables = document.body.querySelectorAll("table.print")
  let tableNum = tables.length
  for(; tableNum--; formatForPrint(tables[tableNum]));
}

const formatForPrint = (table) => {
  const tableParent = table.parentNode
  const cell = table.querySelector("tbody > tr > td")
  if(cell) {
    const topFauxRow = document.createElement("table")
    const fauxRowTable = topFauxRow.insertRow(0).insertCell(0).appendChild(table.cloneNode())
    const colgroup = fauxRowTable.appendChild(document.createElement("colgroup"))
    const headerHider = document.createElement("div")
    const metricsRow = document.createElement("tr")
    const tbods = table.tBodies
    const tbodCount = tbods.length
    let cells = cell.parentNode.cells
    let cellNum = cells.length
    let colCount = 0
    let tbodNum = 0
    let tbod = tbods[0]
    for(; cellNum--; colCount += cells[cellNum].colSpan);
    for(cellNum = colCount; cellNum--; metricsRow.appendChild(document.createElement("td")).style.padding = 0);
    cells = metricsRow.cells
    tbod.insertBefore(metricsRow, tbod.firstChild)
    for(; ++cellNum < colCount; colgroup.appendChild(document.createElement("col")).style.width = cells[cellNum].offsetWidth + "px");
    let borderWidth = metricsRow.offsetHeight
    metricsRow.className = "metricsRow"
    borderWidth -= metricsRow.offsetHeight
    tbod.removeChild(metricsRow)
    tableParent.insertBefore(topFauxRow, table).className = "fauxRow"
    if(table.tHead)
      fauxRowTable.appendChild(table.tHead)
    const fauxRow = topFauxRow.cloneNode(true)
    const fauxRowCell = fauxRow.rows[0].cells[0]
    fauxRowCell.insertBefore(headerHider, fauxRowCell.firstChild).style.marginBottom = -fauxRowTable.offsetHeight - borderWidth + "px"
    if(table.caption)
      fauxRowTable.insertBefore(table.caption, fauxRowTable.firstChild)
    if(tbod.rows[0])
      fauxRowTable.appendChild(tbod.cloneNode()).appendChild(tbod.rows[0])
    for(; tbodNum < tbodCount; tbodNum++) {
      tbod = tbods[tbodNum]
      rows = tbod.rows
      for(; rows[0]; tableParent.insertBefore(fauxRow.cloneNode(true), table).rows[0].cells[0].children[1].appendChild(tbod.cloneNode()).appendChild(rows[0]));
    }
    tableParent.removeChild(table)
  }
  else
    tableParent.insertBefore(document.createElement("div"), table).appendChild(table).parentNode.className="fauxRow"
};